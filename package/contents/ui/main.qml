import QtQuick 2.2
import QtQuick.Controls 2.10 as QQC2
import QtQuick.Layouts 1.11

import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm
import org.kde.kcm 1.2


SimpleKCM {

    ColumnLayout {
        Layout.fillWidth: true

        MobileForm.FormCard {
            Layout.fillWidth: true
            contentItem: ColumnLayout {
                MobileForm.FormTextDelegate {
                    text: "Example"
                }

                Repeater {
                    model: 20
                    MobileForm.FormComboBoxDelegate {
                        text: "Combobox #" + modelData
                        model: ["a", "b", "c", "d", "e", "f"]
                    }
                }
            }
        }
    }
}
