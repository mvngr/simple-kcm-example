#pragma once

#include <KQuickAddons/ManagedConfigModule>

class KcmTest : public KQuickAddons::ManagedConfigModule
{
    Q_OBJECT

public:
    KcmTest(QObject *parent, const KPluginMetaData &data, const QVariantList &args);
};
